const graphql = require('graphql');
const _ = require('lodash');

//import mongoose models
const Question = require('../models/Question');
const Answer = require('../models/Answer');
const User = require('../models/User');


//Define GraphQL properties
const {GraphQLObjectType,
GraphQLString,
GraphQLID,
GraphQLList,
GraphQLSchema
} = graphql;


//Build a schema using GraphQL Schema Language 
//This is the definition of Question Type
const QuestionType = new GraphQLObjectType({
    name: 'Question',
    fields: () => ({
        id: {type: GraphQLID},
        title: {type: GraphQLString}
    })
})

//Answer Type with fields array of options, correct answer and the foreign key, question id.
const AnswerType = new GraphQLObjectType({
    name: 'Answer',
    fields: () => ({
        id: {type: GraphQLID},
        correct: {type: GraphQLString},
        options: {type: new GraphQLList(GraphQLString)},
        //Get the questionId (for QuestionType)
        questions: {
            type: QuestionType,
            resolve(parent, args) {
                console.log(parent);
                return Question.findById(parent.questionId); //Get the questionID
            }
        }
    })
})

//User Type with fields email 
const UserType = new GraphQLObjectType({
    name: 'User',
    fields: () => ({
        id: {type: GraphQLID},
        email: {type: GraphQLString}
    })
})



//Define Root Query => get question by id.
const RootQuery = new GraphQLObjectType({
    name: 'RootQuery',
    fields: {
        //Get Question By its ID
        question: {
            type: QuestionType,
            args: {id: {type: GraphQLID}},
            resolve(parent, args) {
                return Question.findById(args.id);
            }
        },
        //Get All list of questions
        questions: {
            type: new GraphQLList(QuestionType),
            resolve(parent, args) {
               return Question.find({});
            }
        },

        //Get answer by ID
        answer: {
            type: AnswerType,
            args: {id: {type: GraphQLID}},
            resolve(parent, args) {
                return Answer.findById(args.id);
            }
        },

        //Get all answers
        answers: {
            type: new GraphQLList(AnswerType),
            resolve(parent, args) {
                return Answer.find({})
            }
        },

        //Get User by its id
        user: {
            type: UserType,
            args: {id: {type: GraphQLID}},
            resolve(parent, args) {
                return User.findById(args.id)
            }
        },

        users: {
            type: new GraphQLList(UserType),
            resolve(parent, args) {
                return User.find({});
            }
        }
    }
});


//Define GraphQL Mutation
const Mutation = new GraphQLObjectType({
    name: 'Mutation',
    fields: {
        //Mutation to create new questions and save it to database
        addQuestion: {
            type: QuestionType,
            args: {
                title: {type: GraphQLString}
            },
            resolve(parent, args) {
                let question = new Question({
                    title: args.title
                });
                return question.save();
            }
        },
        //Mutation to create new answers and save it to database
        addAnswers: {
            type: AnswerType,
            args: {
                options:{type: new GraphQLList(GraphQLString)},
                correct: {type: GraphQLString},
                questionId: {type: GraphQLID}
            },
            resolve(parent, args) {
                let answer = new Answer({
                    options: args.options,
                    correct: args.correct,
                    questionId: args.questionId
                });
                return answer.save();
            }
        },
        //Create new user and save it to database
        addUser: {
            type: UserType,
            args: {
                email: {type: GraphQLString}
            },
            resolve(parent, args) {
                let user = new User({
                    email: args.email
                });
                return user.save();
            }
        }
    }
})


//Create new schema
module.exports = new GraphQLSchema({
    query: RootQuery,
    mutation: Mutation
})