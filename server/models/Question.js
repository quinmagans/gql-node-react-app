const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Model for the Question Schema
const QuestionSchema = new Schema({
    title: String,
})

module.exports = mongoose.model('Question', QuestionSchema)