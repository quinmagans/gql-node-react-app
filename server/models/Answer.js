const mongoose = require('mongoose');
const Schema = mongoose.Schema;

//Model for the Answer Schema
const AnswerSchema = new Schema({
    options: [String],
    correct: String,
    questionId: String,
    
})

module.exports = mongoose.model('Answer', AnswerSchema)