const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

const {graphqlHTTP} = require('express-graphql');
const schema = require('./schema/schema');

//Use express throughout the application
const app = express();

//This allow a cross-origin requests between server and client
app.use(cors());

//Endpoint for the GraphQL queries
app.use('/graphql', graphqlHTTP({
    schema: schema, 
    graphiql: true,
}));

//Database connection
mongoose.connect('mongodb+srv://admin:passw0rd@b106.nrlsy.mongodb.net/quiz?retryWrites=true&w=majority');
mongoose.connection.once('open', () => {
    console.log("Connected to database...")
})

//PORT listening on
app.listen(4000, () => {
    console.log('Server running on PORT: 4000');
})